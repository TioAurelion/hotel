package org.academiadecodigo.bootcamp;

import org.academiadecodigo.bootcamp.people.*;
import org.academiadecodigo.bootcamp.hotel.*;

public class Main {
    public static void main(String[] args) {
        Hotel california = new Hotel(5,"California");
        People joao = new People("joão", california);
        People a = new People("a", california);
        People b = new People("b", california);
        People c = new People("c", california);
        People d = new People("d", california);
        People e = new People("e", california);

        joao.chekOut();
        joao.checkIn();
        joao.chekOut();
        joao.chekOut();
        joao.whatIsMyKeY();
        joao.chekOut();
        joao.checkIn();
        joao.whatIsMyKeY();
        joao.chekOut();


    }
}
