package org.academiadecodigo.bootcamp.people;

import org.academiadecodigo.bootcamp.hotel.Hotel;

public class People {

    private String name;
    private Hotel hotel;
    private int keY = -1;

    public People(String name, Hotel hotel){
        this.name = name;
        this.hotel = hotel;
    }

    public void checkIn(){
        hotel.checkCheckin(name);
        MyKey();
    }

    public void chekOut(){
        hotel.checkChekOut(name);
        MyKey();
    }

    public void whatIsMyKeY() {
        if(keY >= 0) {
            System.out.println("Your key is number " + keY);
            return;
        }
        System.out.println("You Don't have key, because you don't make check in");
    }

    private void MyKey(){
        keY = hotel.getKeZ();
    }

}
