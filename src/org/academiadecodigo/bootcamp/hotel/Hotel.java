package org.academiadecodigo.bootcamp.hotel;


public class Hotel {

    private Rooms[] rooms;
    private String nameOfHotel;
    private int keY = 0;
    private int KeZ = -1;

    public Hotel(int NumberOfRooms,String nameOfHotel){
        this.nameOfHotel = nameOfHotel;
        rooms = new Rooms[NumberOfRooms];
        addRooms(NumberOfRooms);
    }

    public int getNuberOfRooms() {
        return rooms.length;
    }

    public int getKeZ() {
        return KeZ;
    }

    public int getKeY() {
        return keY;
    }

    public String getNameOfHotel() {
        return nameOfHotel;
    }

    private boolean checkIn(int numberofroom, String nameOfPeople){
        if(rooms[numberofroom].getStatus()){
            System.out.println(nameOfPeople + ", was assigned to the room " + rooms[numberofroom].getNumberofroom());
            rooms[numberofroom].changeStatus();
            return true;
        }
        return false;
    }

    private void checkOut(int numberofroom,String nameOfPeople){
        if(!rooms[numberofroom].getStatus()) {
            System.out.println( nameOfPeople+ ", left the " + rooms[numberofroom].getNumberofroom());
            rooms[numberofroom].changeStatus();
            rooms[numberofroom].checkRoom();
            return;
        }
        System.out.println("Something went wrong.");
    }

    public void checkCheckin(String name){
        if(keY >= 0) {
            for (int i = 0; i < getNuberOfRooms(); i++) {
                if (checkIn(i, name)) {
                    KeZ = i;
                    keY = -1;
                    return;
                }
            }
        }
        System.out.println("You can't make check in more once");
    }

    public void checkChekOut(String name){
        if(KeZ >= 0) {
            checkOut(KeZ, name);
            KeZ = -1;
            keY = 0;
            return;
        }
        System.out.println("You can't do Check Out because you don't make Check In");

    }


    private void addRooms(int NuberOfRooms){
        for (int i=0; i < rooms.length; i++){
            rooms[i] = new Rooms(i);
        }
    }
}
