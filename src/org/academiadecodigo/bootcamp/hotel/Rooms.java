package org.academiadecodigo.bootcamp.hotel;

public class Rooms {

    private int numberofroom;
    private boolean status = true;

    public Rooms(int numberofroom){
        this.numberofroom = numberofroom;
    }

    public boolean getStatus(){
        return status;
    }

    public int getNumberofroom() {
        return numberofroom;
    }

    public void changeStatus(){
        if(this.status){
            this.status = false;
        } else {
            this.status = true;
        }
    }

    public void checkRoom(){
        if(status){
            System.out.println("the room of number " + numberofroom + ", it's free.");
            return;
        }
        System.out.println("sorry the room of number " + numberofroom + ", are you busy");
    }

}
